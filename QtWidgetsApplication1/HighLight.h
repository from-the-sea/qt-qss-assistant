#pragma once
#include <QSyntaxHighlighter>
#include <qtextedit.h>
#include <qtextdocument.h>
#include <QTextCharFormat>
#include <qtextcursor.h>
#include <qhash.h>
#include <qvector.h>
#include <qcolor.h>
#include <qstring.h>
#include <QRegularExpressionMatchIterator>
/*
* 参考于 https://blog.csdn.net/weixin_43554422/article/details/113606586
*/
class HighLight:public QSyntaxHighlighter
{
public:
    struct HighlightingRule
    {
        QString pattern;
        QTextCharFormat format;
    };
    HighLight (QTextDocument *parent = 0);
    void highlightBlock (const QString &text);
    void setTextColor (const QString &str, const QColor &color);
    QVector<HighlightingRule>& getHighlightingRules();
private:
    QVector<HighlightingRule> highlightingRules;
    struct FormatPosition
    {
        int startIndex;
        int endIndex;
    };
    QVector<FormatPosition> IsFormats;
    bool isContain(int start,int end);
};

