#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtWidgetsApplication1.h"
#include "HighLight.h"
#include "keyword.h"
#include <QTreeWidget>

class QtWidgetsApplication1 : public QMainWindow
{
    Q_OBJECT

public:
    QtWidgetsApplication1(QWidget *parent = nullptr);
    ~QtWidgetsApplication1();
    HighLight * highLight;
    KeyWord * keyword;
    QTreeWidget * pop;
    QString cursorText;
    QString lastString;
    bool isAdd;
private slots:
    void on_actionOpenFile_triggered();

    void on_actionRun_triggered();

    void on_actionOutputFile_triggered();

private:
    void do_cursorChanged ();
    void do_textChanged ();
    void connectPlainText();
    void keyPressEvent (QKeyEvent *event);
    void popFrame();
    bool getMatchItems(QString text);
    bool eventFilter (QObject *watched, QEvent *event);
    void choiced();
private:
    Ui::QtWidgetsApplication1Class* ui;
    void intiField (QHash <QString,QStringList>  &styleHash,QHash<QString,QColor> &colors);
};
