#include "keyword.h"


//属性来自qt6/qtbase/src/gui/text/qcssparser.cpp文件下
// 参考于 https://www.cnblogs.com/dongc/p/5512849.html
KeyWord::KeyWord(QObject *parent)
    : QObject{parent}
{
    controls <<
    "QApplication"<<
    "QCheckBox"<<
    "QColumnView"<<
    "QComboBox"<<
    "QCommandLinkButton"<<
    "QDateEdit"<<
    "QDateTimeEdit"<<
    "QDial"<<
    "QDialogButtonBox"<<
    "QDockWidget"<<
    "QDoubleSpinBox"<<
    "QGroupBox"<<
    "QHeaderView"<<
    "QLabel"<<
    "QLineEdit"<<
    "QListView"<<
    "QMenuBar"<<
    "QPlainTextEdit"<<
    "QProgressBar"<<
    "QPushButton"<<
    "QRadioButton"<<
    "QScrollBar"<<
    "QSlider"<<
    "QSpinBox"<<
    "QStackedWidget"<<
    "QStatusBar"<<
    "QTabWidget"<<
    "QTableView"<<
    "QTextEdit"<<
    "QTimeEdit"<<
    "QToolBox"<<
    "QToolButton"<<
    "QTreeView"<<
    "QUndoView"<<
    "QWidget";

    properties<<
    "alternate-background-color"<<
    "background"<<
    "background-attachment"<<
    "background-clip"<<
    "background-color"<<
    "background-image"<<
    "background-origin"<<
    "background-position"<<
    "background-repeat"<<
    "border"<<
    "border-bottom"<<
    "border-bottom-color"<<
    "border-bottom-left-radius"<<
    "border-bottom-right-radius"<<
    "border-bottom-style"<<
    "border-bottom-width"<<
    "border-color"<<
    "border-image"<<
    "border-left"<<
    "border-left-color"<<
    "border-left-style"<<
    "border-left-width"<<
    "border-radius"<<
    "border-right"<<
    "border-right-color"<<
    "border-right-style"<<
    "border-right-width"<<
    "border-style"<<
    "border-top"<<
    "border-top-color"<<
    "border-top-left-radius"<<
    "border-top-right-radius"<<
    "border-top-style"<<
    "border-top-width"<<
    "border-width"<<
    "bottom"<<
    "color"<<
    "float"<<
    "font"<<
    "font-family"<<
    "font-size"<<
    "font-style"<<
    "font-variant"<<
    "font-weight"<<
    "height"<<
    "image"<<
    "image-position"<<
    "left"<<
    "line-height"<<
    "list-style"<<
    "list-style-type"<<
    "margin" <<
    "margin-bottom"<<
    "margin-left"<<
    "margin-right"<<
    "margin-top"<<
    "max-height"<<
    "max-width"<<
    "min-height"<<
    "min-width"<<
    "outline"<<
    "outline-bottom-left-radius"<<
    "outline-bottom-right-radius"<<
    "outline-color"<<
    "outline-offset"<<
    "outline-radius"<<
    "outline-style"<<
    "outline-top-left-radius"<<
    "outline-top-right-radius"<<
    "outline-width"<<
    "padding"<<
    "padding-bottom"<<
    "padding-left"<<
    "padding-right"<<
    "padding-top"<<
    "page-break-after"<<
    "page-break-before"<<
    "position"<<
    "right"<<
    "selection-background-color"<<
    "selection-color"<<
    "spacing"<<
    "subcontrol-origin"<<
    "subcontrol-position"<<
    "text-align"<<
    "text-decoration"<<
    "text-indent"<<
    "text-transform"<<
    "text-underline-style"<<
    "top"<<
    "vertical-align"<<
    "white-space"<<
    "width";


    values <<
    "active"<<
    "scroll"<<
    "fixed "<<
    "alternate-base"<<
    "always"<<
    "auto"<<
    "base"<<
    "bold"<<
    "bottom"<<
    "bright-text"<<
    "button"<<
    "button-text"<<
    "center"<<
    "circle"<<
    "dark"<<
    "dashed"<<
    "decimal"<<
    "disabled"<<
    "disc"<<
    "dot-dash"<<
    "dot-dot-dash"<<
    "dotted"<<
    "double"<<
    "groove"<<
    "highlight"<<
    "highlighted-text"<<
    "inset"<<
    "italic"<<
    "large"<<
    "left"<<
    "light"<<
    "line-through"<<
    "link"<<
    "link-visited"<<
    "lower-alpha"<<
    "lower-roman"<<
    "lowercase"<<
    "medium"<<
    "mid"<<
    "middle"<<
    "midlight"<<
    "native"<<
    "none"<<
    "normal"<<
    "nowrap"<<
    "oblique"<<
    "off"<<
    "on"<<
    "outset"<<
    "overline"<<
    "pre"<<
    "pre-line"<<
    "pre-wrap"<<
    "ridge"<<
    "right"<<
    "selected"<<
    "shadow"<<
    "small" <<
    "small-caps"<<
    "solid"<<
    "square"<<
    "sub"<<
    "super"<<
    "text"<<
    "top"<<
    "transparent"<<
    "underline"<<
    "upper-alpha"<<
    "upper-roman"<<
    "uppercase"<<
    "wave"<<
    "color"<<
    "float"<<
    "font"<<
    "font-family"<<
    "font-size"<<
    "font-style"<<
    "font-variant"<<
    "font-weight"<<
    "height"<<
    "image"<<
    "image-position"<<
    "left"<<
    "line-height"<<
    "list-style"<<
    "list-style-type"<<
    "page-break-after"<<
    "page-break-before"<<
    "position"<<
    "right"<<
    "selection-background-color"<<
    "selection-color"<<
    "spacing"<<
    "subcontrol-origin"<<
    "subcontrol-position"<<
    "text-align"<<
    "text-decoration"<<
    "text-indent"<<
    "text-transform"<<
    "text-underline-style"<<
    "top"<<
    "vertical-align"<<
    "white-space"<<
    "width"<<
    "content"<<
    "no-repeat"<<
    "repeat-x"<<
    "repeat-xy"<<
    "repeat-y"<<
    "repeat"<<
    "round"<<
    "stretch"<<
    "absolute"<<
    "fixed"<<
    "relative"<<
    "static";

    pseudos<<
    "active"<<
    "adjoins-item"<<
    "alternate"<<
    "bottom"<<
    "checked"<<
    "closable"<<
    "closed"<<
    "default"<<
    "disabled"<<
    "edit-focus"<<
    "editable"<<
    "enabled"<<
    "exclusive"<<
    "first"<<
    "flat"<<
    "floatable"<<
    "focus"<<
    "has-children"<<
    "has-siblings"<<
    "horizontal"<<
    "hover"<<
    "indeterminate" <<
    "last"<<
    "left"<<
    "maximized"<<
    "middle"<<
    "minimized"<<
    "movable"<<
    "next-selected"<<
    "no-frame"<<
    "non-exclusive"<<
    "off"<<
    "on"<<
    "only-one"<<
    "open"<<
    "pressed"<<
    "previous-selected"<<
    "read-only"<<
    "right"<<
    "selected"<<
    "top"<<
    "unchecked" <<
    "vertical"<<
    "window";

    subControls <<
    "add-line"<<
    "add-page"<<
    "branch"<<
    "chunk"<<
    "close-button"<<
    "corner"<<
    "down-arrow"<<
    "down-button"<<
    "drop-down"<<
    "float-button"<<
    "groove"<<
    "indicator"<<
    "handle"<<
    "icon"<<
    "item"<<
    "left-arrow"<<
    "left-corner"<<
    "menu-arrow"<<
    "menu-button"<<
    "menu"<<
    "indicator"<<
    "right-arrow"<<
    "pane"<<
    "right-corner"<<
    "scroller"<<
    "section"<<
    "separator"<<
    "sub-line"<<
    "sub-page"<<
    "tab"<<
    "tab-bar"<<
    "tear"<<
    "tearoff"<<
    "text"<<
    "title"<<
    "up-arrow"<<
    "up-button";
    loadUserMacro ();
    if (userMacro.size ()!=0) {
        keywordDictionary["user"] = userMacro;
    }
    keywordDictionary["controls"] = controls;
    keywordDictionary["properties"] = properties;
    keywordDictionary["values"] = values;
    keywordDictionary["pseudos"] = pseudos;
    keywordDictionary["subControls"] = subControls;

    controlColor = QColor(138, 24, 116);
    propertyColor = QColor(46, 89, 167);
    valueColor = QColor(132, 167, 41);
    pseudolColor = QColor(236, 224, 147);
    subControlColor = QColor(230, 0, 18);
    colors["controls"] = controlColor;
    colors["properties"] = propertyColor;
    colors["values"] = valueColor;
    colors["pseudos"] = pseudolColor;
    colors["subControls"] = subControlColor;

    controlIcon = QIcon(":/image/icon/control.png");
    subcontrolIcon = QIcon(":/image/icon/subcontrol.png");
    propertyIcon = QIcon(":/image/icon/property.png");
    valueIcon = QIcon(":/image/icon/value.png");
    pseudosIcon = QIcon(":/image/icon/pseudos.png");
    userIcon = QIcon(":/image/icon/user.png");
    icons["controls"] = controlIcon;
    icons["properties"] = propertyIcon;
    icons["values"] = valueIcon;
    icons["pseudos"] = pseudosIcon;
    icons["subControls"] = subcontrolIcon;
    icons["user"] = userIcon;

}

void KeyWord::addKeyWord()
{

}

QHash <QString,QStringList>& KeyWord::getKeywordDictionary()
{
    return keywordDictionary;
}

QHash<QString,QColor>& KeyWord::getColors()
{
    return colors;
}

QHash<QString, QIcon> &KeyWord::getIcons()
{
    return icons;
}

void KeyWord::loadUserMacro()
{
    QString path = QCoreApplication::applicationDirPath ();
    qDebug()<<"path dir:"<<path;
    QFile file(path+"\\config.json");
    if (file.exists ()) {
        if (file.open (QIODevice::ReadOnly)) {
            QByteArray jsonByte = file.readAll ();
            QJsonDocument jsonDoc(QJsonDocument::fromJson(jsonByte));
            QJsonObject jsonObj = jsonDoc.object ();
            if (jsonObj["openFileHistory"].isArray ()) {
                qDebug()<<"is:"<< "debug";
                QJsonArray jsonArray = jsonObj["openFileHistory"].toArray ();
                qDebug()<<"size:"<<jsonArray.size ();
                for (QJsonArray::const_iterator it = jsonArray.begin (); it !=jsonArray.end (); ++it) {
                    fileHistory<<(*it).toString ();
                }
            }
            if (jsonObj["userMacro"].isObject ()) {
                QJsonObject userObj = jsonObj["userMacro"].toObject ();
                for (QJsonObject::const_iterator it  = userObj.begin (); it != userObj.end (); ++it) {
                    qDebug()<<"value:"<<it.value ().toString ()<<"key:"<<it.key ();
                    userMacro.append (it.key ());
                    userTable[it.key ()] = it.value ().toString ();
                }
            }
        }
    }
}
/*
 *
 *属性
     "alternate-background-color",
     "background",
     "background-attachment",
     "background-clip",
     "background-color",
     "background-image",
     "background-origin",
     "background-position",
     "background-repeat",
     "border",
     "border-bottom",
     "border-bottom-color",
     "border-bottom-left-radius",
     "border-bottom-right-radius",
     "border-bottom-style",
     "border-bottom-width",
     "border-color",
     "border-image",
     "border-left",
     "border-left-color",
     "border-left-style",
     "border-left-width",
     "border-radius",
     "border-right",
     "border-right-color",
     "border-right-style",
     "border-right-width",
     "border-style",
     "border-top",
     "border-top-color",
     "border-top-left-radius",
     "border-top-right-radius",
     "border-top-style",
     "border-top-width",
     "border-width",
     "bottom",
     "color",
     "float",
     "font",
     "font-family",
     "font-size",
     "font-style",
     "font-variant",
     "font-weight",
     "height",
     "image",
     "image-position",
     "left",
     "line-height",
     "list-style",
     "list-style-type",
     "margin" ,
     "margin-bottom",
     "margin-left",
     "margin-right",
     "margin-top",
     "max-height",
     "max-width",
     "min-height",
     "min-width",
     "outline",
     "outline-bottom-left-radius",
     "outline-bottom-right-radius",
     "outline-color",
     "outline-offset",
     "outline-radius",
     "outline-style",
     "outline-top-left-radius",
     "outline-top-right-radius",
     "outline-width",
     "padding",
     "padding-bottom",
     "padding-left",
     "padding-right",
     "padding-top",
     "page-break-after",
     "page-break-before",
     "position",
     "right",
     "selection-background-color",
     "selection-color",
     "spacing",
     "subcontrol-origin",
     "subcontrol-position",
     "text-align",
     "text-decoration",
     "text-indent",
     "text-transform",
     "text-underline-style",
     "top",
     "vertical-align",
     "white-space",
     "width"
/*
 * 值
 *    "active",
 *    "scroll",
 *    "fixed ",
     "alternate-base",
     "always",
     "auto",
     "base",
     "bold",
     "bottom",
     "bright-text",
     "button",
     "button-text",
     "center",
     "circle",
     "dark",
     "dashed",
     "decimal",
     "disabled",
     "disc",
     "dot-dash",
     "dot-dot-dash",
     "dotted",
     "double",
     "groove",
     "highlight",
     "highlighted-text",
     "inset",
     "italic",
     "large",
     "left",
     "light",
     "line-through",
     "link",
     "link-visited",
     "lower-alpha",
     "lower-roman",
     "lowercase",
     "medium",
     "mid",
     "middle",
     "midlight",
     "native",
     "none",
     "normal",
     "nowrap",
     "oblique",
     "off",
     "on",
     "outset",
     "overline",
     "pre",
     "pre-line",
     "pre-wrap",
     "ridge",
     "right",
     "selected",
     "shadow",
     "small" ,
     "small-caps",
     "solid",
     "square",
     "sub",
     "super",
     "text",
     "top",
     "transparent",
     "underline",
     "upper-alpha",
     "upper-roman",
     "uppercase",
     "wave",
     "window",
     "window-text",
     "x-large",
     "xx-large", Value_XXLarge
     "background-color",
     "background-image",
     "background-origin",
     "background-position",
     "background-repeat",
     "border",
     "border-bottom",
     "border-bottom-color",
     "border-bottom-left-radius",
     "border-bottom-right-radius",
     "border-bottom-style",
     "border-bottom-width",
     "border-color",
     "border-image",
     "border-left",
     "border-left-color",
     "border-left-style",
     "border-left-width",
     "border-radius",
     "border-right",
     "border-right-color",
     "border-right-style",
     "border-right-width",
     "border-style",
     "border-top",
     "border-top-color",
     "border-top-left-radius",
     "border-top-right-radius",
     "border-top-style",
     "border-top-width",
     "border-width",
     "bottom",
     "color",
     "float",
     "font",
     "font-family",
     "font-size",
     "font-style",
     "font-variant",
     "font-weight",
     "height",
     "image",
     "image-position",
     "left",
     "line-height",
     "list-style",
     "list-style-type",
     "margin" ,
     "margin-bottom",
     "margin-left",
     "margin-right",
     "margin-top",
     "max-height",
     "max-width",
     "min-height",
     "min-width",
     "outline",
     "outline-bottom-left-radius",
     "outline-bottom-right-radius",
     "outline-color",
     "outline-offset",
     "outline-radius",
     "outline-style",
     "outline-top-left-radius",
     "outline-top-right-radius",
     "outline-width",
     "padding",
     "padding-bottom",
     "padding-left",
     "padding-right",
     "padding-top",
     "page-break-after",
     "page-break-before",
     "position",
     "right",
     "selection-background-color",
     "selection-color",
     "spacing",
     "subcontrol-origin",
     "subcontrol-position",
     "text-align",
     "text-decoration",
     "text-indent",
     "text-transform",
     "text-underline-style",
     "top",
     "vertical-align",
     "white-space",
     "width",
     "border",
     "content",
     "margin",
     "padding",
     "no-repeat",
     "repeat-x",
     "repeat-xy",
     "repeat-y",
     "repeat",
     "round",
     "stretch",
     "absolute",
     "fixed",
     "relative",
     "static"
 * /
 * 伪类器
 *
 *   "active",
     "adjoins-item",
     "alternate",
     "bottom",
     "checked",
     "closable",
     "closed",
     "default",
     "disabled",
     "edit-focus",
     "editable",
     "enabled",
     "exclusive",
     "first",
     "flat",
     "floatable",
     "focus",
     "has-children",
     "has-siblings",
     "horizontal",
     "hover",
     "indeterminate" ,
     "last",
     "left",
     "maximized",
     "middle",
     "minimized",
     "movable",
     "next-selected",
     "no-frame",
     "non-exclusive",
     "off",
     "on",
     "only-one",
     "open",
     "pressed",
     "previous-selected",
     "read-only",
     "right",
     "selected",
     "top",
     "unchecked" ,
     "vertical",
     "window"
 * /
 * 子控件
 *"add-line",
"add-page",
"branch",
"chunk",
"close-button",
"corner",
"down-arrow",
"down-button",
"drop-down",
"float-button",
"groove",
"indicator",
"handle",
"icon",
"item",
"left-arrow",
"left-corner",
"menu-arrow",
"menu-button",
"menu",-
"indicator",
"right-arrow",
"pane",
"right-corner",
"scroller",
"section",
"separator",
"sub-line",
"sub-page",
"tab",
"tab-bar",
"tear",
"tearoff",
"text",
"title",
"up-arrow",
"up-button"
*/
