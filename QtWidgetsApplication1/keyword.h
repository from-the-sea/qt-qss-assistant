#pragma once

#include <QObject>
#include <QHash>
#include <QColor>
#include <QIcon>
#include <QCoreApplication>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
class KeyWord : public QObject
{
    Q_OBJECT
public:
    explicit KeyWord(QObject *parent = nullptr);
    void addKeyWord();
    void searchSimilarKeyWord();
    QHash <QString,QStringList>& getKeywordDictionary();
    QHash <QString,QColor>& getColors();
    QHash <QString,QIcon> & getIcons();
    QHash <QString,QString> userTable;
    QStringList fileHistory;
private :
    QStringList properties;
    QStringList values;
    QStringList pseudos;
    QStringList subControls;
    QStringList controls;
    QStringList userMacro;
    QColor controlColor;
    QColor subControlColor;
    QColor propertyColor;
    QColor valueColor;
    QColor pseudolColor;
    QIcon  controlIcon;
    QIcon  subcontrolIcon;
    QIcon  propertyIcon;
    QIcon  valueIcon;
    QIcon  pseudosIcon;
    QIcon  userIcon;
    QHash <QString,QColor> colors;
    QHash <QString,QStringList>keywordDictionary;
    QHash <QString,QIcon> icons;
    void loadUserMacro();
};

